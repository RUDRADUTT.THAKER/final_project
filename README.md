# final_project

The final project for AOS573

File Descriptioin:

**AOS573_data_f.nc** : MERRA2 reanalysis dataset for the AOS573 project. The dataset contains monthly averaged datafiles and following variables:   Integrated Water Vapor(IWV), Integrated Vapor Transport (IVT), Meridional and Zonal Integrated Vapor Transport (vIVT and uIVT). The data was provided by Dr. Christine Shields which was downloaded and saved for the Atmospheric River Tracking Method Intercomparison Project (ARTMIP) https://opensky.ucar.edu/islandora/object/articles:23127

**AOS_573_code.ipynb** : The code is developed to make individual monthly dataset and then plot the monthly trends of each variables from the dataset. It also calculates and plots the 95% significance level to see the significant trend.

**Ru_env.yml** : This is contains all the dependecies and packages/modules used to run the code and also do further data processing and data analysis.
